<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>

		</div><!-- #main -->
		<footer id="colophon" class="site-footer" role="contentinfo">
			<?php //get_sidebar( 'main' ); ?>
			<div class="site-info">
				
			<div class="col col_first">
				<h2><strong>Nous partager sur les réseaux sociaux</strong></h2><br />
				<!--<a class="facebook" style="background: none;" target="blank" href="https://www.facebook.com/"><img width="20%" alt="facebook" src="<?php //echo get_template_directory_uri(); ?>/images/facebook.png"></a>
				<a class="youtube" target="blank" href="https://www.youtube.com/"><img width="20%" alt="youtube" src="<?php //echo get_template_directory_uri(); ?>/images/youtube.png"></a>
				<a class="" target="blank" href="#"><img width="20%" alt="youtube" src="<?php //echo get_template_directory_uri(); ?>/images/wordpress.png"></a>
				<a target="blank" href="#"><img width="15%" alt="scoopit" src="<?php //echo get_template_directory_uri(); ?>/images/scoopit.png" style="margin-left: 5px;margin-top: 2px; margin-bottom: 8px"></a>-->
                
                
                <a class="addthis_button" href="http://www.addthis.com/bookmark.php?v=250&amp;pub=xa-4ab3b0412ad55820">
                <img width="20%" alt="facebook" src="<?php echo get_template_directory_uri(); ?>/images/facebook.png">
                <img width="20%" alt="youtube" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                <img width="20%" alt="youtube" src="<?php echo get_template_directory_uri(); ?>/images/wordpress.png">
                <img width="15%" alt="scoopit" src="<?php echo get_template_directory_uri(); ?>/images/scoopit.png" style="margin-left: 5px;margin-top: 2px; margin-bottom: 8px">
                </a><script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js?pub=xa-4ab3b0412ad55820"></script>
                
                
                <h2><strong>Pratiques &amp; mentions</strong></h2>
				<ul>
					<li><a href="http://www.vanille-naturelle.com/sitemap.xml">Plan du site</a></li>
					<!--<li><a href="javascript:void(0);">Mentions légales</a></li>-->
				</ul>
                
				
			</div>
			<div class="col col_center">
				<h2><strong>Nous contacter & Nous situer</strong></h2>
					<ul>
                        <li><a href="http://www.vanille-naturelle.com/nous-contacter/">Contact</a></li>
                        <li><a href="http://www.vanille-naturelle.com/nos-implantations/">Notre implantation</a></li>
                        <li><a href="http://www.vanille-naturelle.com/category/news/">News</a></li>
					</ul>
                
			</div>
						<!-- <div class="col">
				<h2><strong>STOI</strong> Société Trading de l'Océan Indien.</h2>
				<p>Rester informé de l’actualité du secteur                         <a href="http://www.vanille-naturelle.com">STOI</a></p>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-top:18px;">
                  <tr>
                    <td><img src="<?php echo get_template_directory_uri(); ?>/images/certification_stoi_logo.png" width="150" alt="certificat" /></td>
                    <td><img src="<?php echo get_template_directory_uri(); ?>/images/global_gap.png"  height="65" alt="globalgap" /></td>
                  </tr>
                </table>
			</div> 	-->
			
				<?php //wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'foot-menu' ) ); ?>
				<!--© 2005-2007 Stoi Agri, Tous droits réservés -->
				<?php //do_action( 'twentythirteen_credits' ); ?>
				<!-- a  href="<?php echo esc_url( __( 'http://wordpress.org/', 'twentythirteen' ) ); ?>" title="<?php esc_attr_e( 'Semantic Personal Publishing Platform', 'twentythirteen' ); ?>"><?php printf( __( 'Proudly powered by %s', 'twentythirteen' ), 'WordPress' ); ?></a -->
			</div><!-- .site-info -->
		</footer><!-- #colophon -->
	</div><!-- #page -->

	<?php wp_footer(); ?>
</body>
</html>