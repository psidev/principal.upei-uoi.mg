<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div class="left_cnt">
			<div id="left-menu" class="left-menu">
				<h3 class="title block-title">STOI GROUP</h3>
				<div id="cnt-left-menu" class="cnt-left-menu">
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'left-menu-liste' ) ); ?>
					<!-- ul>
						<li><a href="#">Notre Histoire</a></li>
						<li><a href="#">Nos Valeurs</a></li>
						<li><a href="#">Nos Engagements</a></li>
						<li><a href="#">Nos Partenaires</a></li>
						<li><a href="#">Nos Trophées</a></li>
					</ul -->
				</div>
			</div>
			<div class="left_cnt_white">
            <div class="block_list_prod">
				<div id="jslidernews3" class="lof-slidecontent" style="width:214px; height:215px;">
					<div class="preload"><div></div></div>						
					<div  class="button-previous">Previous</div>								   
					<!-- MAIN CONTENT --> 
					<div class="main-slider-content" style="width:214px; height:215px;">
						<!-- <ul class="sliders-wrap-inner">
							<li>
								<a href="http://www.vanille-naturelle.com/nos-produits/nos-grains-secs/lingot-blanc/">
									<img src="<?php echo get_template_directory_uri(); ?>/images/haricot.jpg" alt="haricot blan" />  
									<div class="slider-description">
										<p>Haricot blanc</p>
									 </div>
								</a>
							</li> 			
							<li>
								<a href="http://www.vanille-naturelle.com/nos-produits/nos-grains-secs/haricot-rouge/">
									<img src="<?php echo get_template_directory_uri(); ?>/images/arachide.jpg" alt="Arachide rouge/rose" />  
									<div class="slider-description">
										<p>Arachide rouge/rose</p>
									 </div>
								</a>
							</li> 				
							<li>
								<a href="http://www.vanille-naturelle.com/nos-produits/nos-grains-secs/black-eyes/">
									<img src="<?php echo get_template_directory_uri(); ?>/images/black-eyes.jpg" alt="black eyes" />  
									<div class="slider-description">
										<p>Black eyes</p>
									 </div>
								</a>
							</li> 								
						</ul>  	 -->
					</div>
						   <!-- END MAIN CONTENT --> 
					   
					<div class="button-next">Next</div>
				</div> 
				<!-- a href="<?php //echo home_url( '/' ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/haricot.jpg" alt="haricot blan" /></a>
				<p>Haricot blanc</p -->
			</div>
            </div>
            
            
            <div class="small_block_middle_infos_news_txt">
            	<div class="small_block_middle_infos_news_txt_title">NEWS</div>
                <div class="small_block_middle_infos_news_txt_content">
                <ul style="margin-left: 12px; padding: 0px; margin-top:0px; margin-bottom:0px;">
						<?php
                        wp_reset_query();
                        $postslist_news = get_posts('showposts=4&category_name=NEWS&order=DESC&orderby=date');		
                        foreach($postslist_news as $post_news) :
                            ?><a href="<?php echo get_permalink( $post_news->ID );?>" title="" style="font-weight:bold; color:#000000;"><?php
                            echo "<li>".truncate_william_func(strip_tags($post_news->post_title), 70,$etc = " ...")."</li>";
                            ?></a><?php
                        endforeach; 
                        wp_reset_query(); 
                        ?>
                </ul>
                </div>
            </div>
            
            <div class="small_block_middle_infos_news_img">
            	<div class="small_block_middle_infos_news_img_title">InfoAgriBio</div>
                <div class="small_block_middle_infos_news_img_contetnt">
					<?php
                    wp_reset_query();
                    $postslist_agribio = get_posts('showposts=1&category_name=InfosAgriBio&order=DESC&orderby=date');		
                    //fb($postslist_agribio);
                    foreach($postslist_agribio as $post_agribio) :
                        ?>
                        <div>
                        <a href="<?php echo get_permalink( $post_agribio->ID );?>">
                        <h6 style="margin:0px; text-align:center;"><?php echo $post_agribio->post_title;?></h6>
                        <?php
                        echo get_the_post_thumbnail( $post_agribio->ID, "thumbnail",array( 'class' => 'aligncenter' ));
                        //fb(get_permalink( $post_agribio->ID ));
                        ?>
                        </a>
                        </div>
                        <?php 
                    endforeach; 
                    wp_reset_query(); 
                    ?>
                </div>
            </div>
            
            <?php
			wp_reset_query();
			$postslist_stoiastuces = get_posts('showposts=1&category_name=StoiAstuces&order=DESC&orderby=date');		
			foreach($postslist_stoiastuces as $post_stoiastuces) :
			?>
            <div class="smal_block_divers_stoi">
            	<div class="smal_block_divers_stoi_head">&nbsp;</div>
				<div class="smal_block_divers_stoi_content">
                <a href="<?php echo get_permalink( $post_stoiastuces->ID );?>" style="text-decoration:none; text-align:center;" title="<?php echo $post_stoiastuces->post_title;?>">
				<?php
                echo get_the_post_thumbnail( $post_stoiastuces->ID, "thumbnail",array( 'class' => 'aligncenter' ));
                ?>
                </a>
                </div>
            </div>
            <?php 
			endforeach; 
			wp_reset_query(); 
			?>
            
		</div>
		
		<div id="content" class="site-content" role="main">

		<?php if ( function_exists('yoast_breadcrumb') ) {
		  yoast_breadcrumb('<p id="breadcrumbs">','</p>');
		} ?>
			
			
			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', get_post_format() ); ?>
				<?php twentythirteen_post_nav(); ?>
				<?php //comments_template(); ?>

			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>