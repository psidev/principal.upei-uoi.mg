<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

include ("home-header.php") ;  ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<div class="deco"></div>
			<div class="block_prod prod_1">
				<div class="left_prod">
					<h2 class="titre_prod"><a href="<?php echo home_url( '/' ); ?>">Nos Vanilles</a></h2>
					<div class="img_prod">
						<a href="javascript:void(0);<?php //echo home_url( '/' ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/vanille.jpg" alt="Nos vanilles" /></a>
					</div>
				</div>
				<div class="right_prod">
					<ul>
						<!--<?php
					/*global $post;
					$postslist = get_posts('showposts=8&category_name=nos-vanille&order=ASC&orderby=date');						
					foreach($postslist as $post) :*/
				?>
						<li><a href="<?php //the_permalink() ?>"><?php //the_title(); ?></a></li>
						<?php 
						/*endforeach; 
						wp_reset_query(); */
						?>-->
                        
<li><a href="http://www.stoiagri.com/nos-produits/nos-vanilles/nos-plantations-et-zones-de-collecte/">Nos Plantations</a></li>
<li><a href="http://www.stoiagri.com/nos-produits/nos-vanilles/notre-preparation/">Notre Pr&eacute;paration</a></li>
<li><a href="http://www.stoiagri.com/nos-produits/nos-vanilles/nos-varietes/">Nos Vari&eacute;t&eacute;s</a></li>
<li><a href="http://www.stoiagri.com/nos-produits/nos-vanilles/notre-conditionnement-et-livraison/">Notre Conditionnement</a></li>
<li><a href="http://www.stoiagri.com/nos-produits/nos-vanilles/bilan/">Bilan</a></li>
					</ul>
				</div>				
			</div>
			<div class="block_prod prod_2">
				<div class="left_prod">
					<h2 class="titre_prod"><a href="<?php echo home_url( '/' ); ?>">Nos Litchi</a></h2>
					<div class="img_prod">
						<a href="javascript:void(0);<?php //echo home_url( '/' ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/litchis.jpg" alt="Nos Litchi" /></a>
					</div>
				</div>
				<div class="right_prod">
					<ul>
						<!--<?php
					/*global $post;
					$postslist = get_posts('showposts=8&category_name=nos-litchi&order=ASC&orderby=date');						
					foreach($postslist as $post) :*/
				?>
						<li><a href="<?php //the_permalink() ?>"><?php //the_title(); ?></a></li>
						<?php 
						/*endforeach; 
						wp_reset_query(); */
						?>-->
                        
<li><a href="http://www.stoiagri.com/nos-produits/nos-litchis/nos-plantations/">Nos Plantations</a></li>
<li><a href="http://www.stoiagri.com/nos-produits/nos-litchis/notre-station/">Notre Station</a></li>
<li><a href="http://www.stoiagri.com/nos-produits/nos-litchis/nos-varietes/">Nos Vari&eacute;t&eacute;s</a></li>
<li><a href="http://www.stoiagri.com/nos-produits/nos-litchis/fiche-technique/">Notre Fiche Technique</a></li>
<li><a href="http://www.stoiagri.com/nos-produits/nos-litchis/nos-circuits/">Nos Circuits</a></li>
<li><a href="http://www.stoiagri.com/nos-produits/nos-litchis/bilan/">Bilan</a></li>

                        
					</ul>
				</div>				
			</div>
			<div class="block_list_prod">
				<div id="jslidernews3" class="lof-slidecontent" style="width:214px; height:215px;">
					<div class="preload"><div></div></div>						
					<div  class="button-previous">Previous</div>								   
					<!-- MAIN CONTENT --> 
					<div class="main-slider-content" style="width:214px; height:215px;">
						<ul class="sliders-wrap-inner">
							<li>
								<a href="http://www.stoiagri.com/nos-produits/nos-grains-secs/lingot-blanc/">
									<img src="<?php echo get_template_directory_uri(); ?>/images/haricot.jpg" alt="haricot blan" />  
									<div class="slider-description">
										<p>Haricot blanc</p>
									 </div>
								</a>
							</li> 			
							<li>
								<a href="http://www.stoiagri.com/nos-produits/nos-grains-secs/haricot-rouge/">
									<img src="<?php echo get_template_directory_uri(); ?>/images/arachide.jpg" alt="Arachide rouge/rose" />  
									<div class="slider-description">
										<p>Arachide rouge/rose</p>
									 </div>
								</a>
							</li> 				
							<li>
								<a href="http://www.stoiagri.com/nos-produits/nos-grains-secs/black-eyes/">
									<img src="<?php echo get_template_directory_uri(); ?>/images/black-eyes.jpg" alt="black eyes" />  
									<div class="slider-description">
										<p>Black eyes</p>
									 </div>
								</a>
							</li> 								
						</ul>  	
					</div>
						   <!-- END MAIN CONTENT --> 
					   
					<div class="button-next">Next</div>
				</div> 
				<!-- a href="<?php //echo home_url( '/' ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/haricot.jpg" alt="haricot blan" /></a>
				<p>Haricot blanc</p -->
			</div>
			<div class="block_middle">
				<?php 	
					global $post;
					$postslist = get_posts('meta_key=home_taroka&meta_value=texte_taroka');	
					foreach($postslist as $post) : setup_postdata($post); 
				?>
				<div class="block_middle_img">
					<a href="javascript:void(0);<?php //the_permalink() ?>" class="fancybox" title="<?php the_title_attribute(); ?>" >
						<?php  // post image � la une
							the_post_thumbnail('large');
						?>	
					</a>
				</div>
				<div class="block_middle_txt">
					<h2><?php the_title(); ?></h2>
					<?php the_content(); ?>
					<a href="http://www.stoiagri.com/nos-produits/notre-fertilisant-taroka/definition/<?php //the_permalink(); ?>" class="suite" title="<?php the_title_attribute(); ?>" >Lire la suite
					</a>
				</div>
				<?php endforeach; ?>
			</div>
		</div><!-- #content -->
	</div><!-- #primary -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>