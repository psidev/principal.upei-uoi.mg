<?php
/**
 * The template for displaying Category pages
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		
        <div class="left_cnt">
			<div id="left-menu" class="left-menu">
				<h3 class="title block-title">STOI GROUP</h3>
				<div id="cnt-left-menu" class="cnt-left-menu">
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'left-menu-liste' ) ); ?>
					<!-- ul>
						<li><a href="#">Notre Histoire</a></li>
						<li><a href="#">Nos Valeurs</a></li>
						<li><a href="#">Nos Engagements</a></li>
						<li><a href="#">Nos Partenaires</a></li>
						<li><a href="#">Nos Trophées</a></li>
					</ul -->
				</div>
			</div>
			<div class="block_list_prod">
				<div id="jslidernews3" class="lof-slidecontent" style="width:214px; height:215px;">
					<div class="preload"><div></div></div>						
					<div  class="button-previous">Previous</div>								   
					<!-- MAIN CONTENT --> 
					<div class="main-slider-content" style="width:214px; height:215px;">
						<ul class="sliders-wrap-inner">
							<li>
								<a href="http://www.vanille-naturelle.com/nos-produits/nos-grains-secs/lingot-blanc/">
									<img src="<?php echo get_template_directory_uri(); ?>/images/haricot.jpg" alt="haricot blan" />  
									<div class="slider-description">
										<p>Haricot blanc</p>
									 </div>
								</a>
							</li> 			
							<li>
								<a href="http://www.vanille-naturelle.com/nos-produits/nos-grains-secs/haricot-rouge/">
									<img src="<?php echo get_template_directory_uri(); ?>/images/arachide.jpg" alt="Arachide rouge/rose" />  
									<div class="slider-description">
										<p>Arachide rouge/rose</p>
									 </div>
								</a>
							</li> 				
							<li>
								<a href="http://www.vanille-naturelle.com/nos-produits/nos-grains-secs/black-eyes/">
									<img src="<?php echo get_template_directory_uri(); ?>/images/black-eyes.jpg" alt="black eyes" />  
									<div class="slider-description">
										<p>Black eyes</p>
									 </div>
								</a>
							</li> 								
						</ul>  	
					</div>
						   <!-- END MAIN CONTENT --> 
					   
					<div class="button-next">Next</div>
				</div> 
				<!-- a href="<?php //echo home_url( '/' ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/haricot.jpg" alt="haricot blan" /></a>
				<p>Haricot blanc</p -->
			</div>
		</div>
        
        <div id="content" class="site-content" role="main">

		<?php if ( have_posts() ) : ?>
			<header class="archive-header" style="margin-top:15px;">
				<!--<h1 class="archive-title"><?php //printf( __( 'Category Archives: %s', 'twentythirteen' ), single_cat_title( '', false ) ); ?></h1>-->

				<?php if ( category_description() ) : // Show an optional category description ?>
				<div class="archive-meta"><?php //echo category_description(); ?></div>
				<?php endif; ?>
			</header><!-- .archive-header -->

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>

			<?php twentythirteen_paging_nav(); ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>