<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div class="left_cnt">
			<div id="left-menu" class="left-menu">
				<h3 class="title block-title">STOI GROUP</h3>
				<div id="cnt-left-menu" class="cnt-left-menu">
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'left-menu-liste' ) ); ?>
					<!-- ul>
						<li><a href="#">Notre Histoire</a></li>
						<li><a href="#">Nos Valeurs</a></li>
						<li><a href="#">Nos Engagements</a></li>
						<li><a href="#">Nos Partenaires</a></li>
						<li><a href="#">Nos Trophées</a></li>
					</ul -->
				</div>
			</div>
			<div class="block_list_prod">
				<div id="jslidernews3" class="lof-slidecontent" style="width:214px; height:215px;">
					<div class="preload"><div></div></div>						
					<div  class="button-previous">Previous</div>								   
					<!-- MAIN CONTENT --> 
					<div class="main-slider-content" style="width:214px; height:215px;">
						<ul class="sliders-wrap-inner">
							<li>
								<a href="http://www.vanille-naturelle.com/nos-produits/nos-grains-secs/lingot-blanc/">
									<img src="<?php echo get_template_directory_uri(); ?>/images/haricot.jpg" alt="haricot blan" />  
									<div class="slider-description">
										<p>Haricot blanc</p>
									 </div>
								</a>
							</li> 			
							<li>
								<a href="http://www.vanille-naturelle.com/nos-produits/nos-grains-secs/haricot-rouge/">
									<img src="<?php echo get_template_directory_uri(); ?>/images/arachide.jpg" alt="Arachide rouge/rose" />  
									<div class="slider-description">
										<p>Arachide rouge/rose</p>
									 </div>
								</a>
							</li> 				
							<li>
								<a href="http://www.vanille-naturelle.com/nos-produits/nos-grains-secs/black-eyes/">
									<img src="<?php echo get_template_directory_uri(); ?>/images/black-eyes.jpg" alt="black eyes" />  
									<div class="slider-description">
										<p>Black eyes</p>
									 </div>
								</a>
							</li> 								
						</ul>  	
					</div>
						   <!-- END MAIN CONTENT --> 
					   
					<div class="button-next">Next</div>
				</div> 
				<!-- a href="<?php //echo home_url( '/' ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/haricot.jpg" alt="haricot blan" /></a>
				<p>Haricot blanc</p -->
			</div>
		</div>
		<div id="content" class="site-content" role="main">

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
						<div class="entry-thumbnail">
							<?php the_post_thumbnail(); ?>
						</div>
						<?php endif; ?>

						<h1 class="entry-title"><?php the_title(); ?></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					</div><!-- .entry-content -->

					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-meta -->
				</article><!-- #post -->

				<?php //comments_template(); ?>
			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>