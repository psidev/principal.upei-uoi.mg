<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php //language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php //language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php //language_attributes(); ?>translate="no">
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
    
    <meta content="index,follow,all" name="robots">
    
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/camera.css" type="text/css" media="screen"> 
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style6.css" />
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/camera.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/script.js"></script>
	<script>
		jQuery(document).ready(function(){   
              jQuery('.camera_wrap').camera();
			  
			var buttons = { previous:jQuery('#jslidernews3 .button-previous') ,
							next:jQuery('#jslidernews3 .button-next') };
	  
			var _complete = function(slider, index){ 
									jQuery('#jslidernews3 .slider-description').animate({height:0});
									slider.find(".slider-description").animate({height:100}) 
							};
			jQuery('#jslidernews1').lofJSidernews( { interval : 3000,
				direction		: 'opacity',	
				easing			: 'easeOutBounce',
				duration		: 1000,
				auto		 	: true,
				mainWidth:183,
				//buttons			: buttons,
				//onComplete:_complete 
			});	
			jQuery('#jslidernews2').lofJSidernews( { interval : 3500,
				direction		: 'opacity',	
				easing			: 'easeOutBounce',
				duration		: 1100,
				auto		 	: true,
				mainWidth:183,
				//buttons			: buttons,
				//onComplete:_complete 
			});	
			jQuery('#jslidernews3').lofJSidernews( { interval : 4000,
				direction		: 'opacity',	
				easing			: 'easeOutBounce',
				duration		: 1200,
				auto		 	: true,
				mainWidth:214,
				buttons			: buttons,
				onComplete:_complete 
			});	
        });    
	</script>
    <meta name="google-translate-customization" content="efb0f63ba785062f-c42e382e0cfbe856-g40109104eb3ff1de-10"/>
</head>

<body <?php body_class(); ?>>

<div style=" display: none; height:0px;">
Vanille Naturelle - STOI - Soci&eacute;t&eacute; Trading de l'Oc&eacute;an Indien, la STOI sp&eacute;cialis&eacute;e dans l&acute;agriculture biologique depuis 1997 avec activit&eacute; principale le respect de la terre, des producteurs, de la nature et des consommateurs, avec production du fertilisant biologique Taroka
<br />
vanille,naturelle,gousse,vanille bio,madagascar,vanille madagascar,agriculture,bio ,biologique,importation,agriculture biologique,terre,producteur,agriculteur,fertilisant biologique,fertilisant,plantation,grain sec,litchis,haricot,lingot blanc, haricot blanc, haricoot rouge,haricoot rouge marbr&eacute;,black eyes, arachides, zezika, taroka, globalgap, engrais,ecocert
<br />
Depuis 1999, elle a d&eacute;velopp&eacute; des plantations de vanille, de litchis, de girofle, de pomme de terre, de l&acute;ananas victoria, de piment dans les r&eacute;gions de Tamatave et d&acute;Antsirabe.
<br />
La STOI-AGRI est la premi&egrave;re &agrave; Madagascar &agrave; certifier leurs produits (fertilisants, litchis) en tant que produits biologiques aupr&egrave;s de l&acute;ECOCERT INTERNATIONAL.
<br />
Depuis 2006, sa production de vanille est &eacute;galement certifi&eacute;e bio.
<br />
Les plantations de litchis en partenariat avec des producteurs sont aussi certifi&eacute;es Global Gap &agrave; partir de 2007.
<br />
Durant plus de quinze ans, la STOI s&acute;est attach&eacute;e &agrave; promouvoir la diversit&eacute; des produits &agrave; Madagascar, la qualit&eacute; des produits r&eacute;gionaux, le savoir-faire des hommes dans le respect des cultures et traditions locales pour offrir au plus grand nombre ce que la nature peut donner de meilleur.
<br />
&quot; Le travail de la terre demande beaucoup d&acute;efforts. La collecte, le triage n&eacute;cessitent beaucoup de travail. Les conditions de travail &agrave; Madagascar ne sont pas toujours faciles. C&acute;est avec d&eacute;termination que beau temps mauvais temps, l&acute;&eacute;quipe met du c&oelig;ur &agrave; l&acute;ouvrage. Leurs efforts sont r&eacute;compens&eacute;s : leur plus grande satisfaction, ils l&acute;obtiennent quand ils voient le produit final sur les palettes. Le sentiment d&acute;accomplissement et de fiert&eacute; qu&acute;ils en tirent est une grande source de motivation &quot;
<br />
<?php
	global $post;
	$postslist_news = get_posts('showposts=1&category_name=NEWS&order=DESC&orderby=date');		
	//fb($postslist_news);
	foreach($postslist_news as $post_news) :
		echo $post_news->post_title."<br/>".$post_news->post_content;
	endforeach; 
	wp_reset_query(); 
?>
<hr/>
<?php
	global $post;
	$postslist_agribio = get_posts('showposts=1&category_name=InfosAgriBio&order=DESC&orderby=date');		
	//fb($postslist_agribio);
	foreach($postslist_agribio as $post_agribio) :
		echo $post_agribio->post_title."<br/>".$post_agribio->post_content;
	endforeach; 
	wp_reset_query(); 
?>
</div>


	<div id="page" class="hfeed site">
		<header id="masthead" class="site-header" role="banner">			
			<div id="home_slider" >
				<div class="slider span8"> 
					<!-- Debut de #camera_wrap_1 -->
					<div class="camera_wrap camera_azure_skin" id="camera_wrap_1">
						<?php
							global $post;
							$postslist = get_posts('showposts=8&category_name=slide-home&order=DESC&orderby=date');		
							
							//fb($postslist);
											
							foreach($postslist as $post) :
						?>	
								<div data-thumb="<?php echo wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); ?>" data-src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); ?>">
									<div class="camera_caption fadeFromBottom" style="padding-bottom: 100px;padding-left: 600px;">	
										<!--<h3><?php //the_title(); ?></h3>-->
										<?php echo $post->post_content; ?>
									</div>
								</div>
							<?php endforeach; 
								wp_reset_query(); 
							?>
					</div><!-- Fin de #camera_wrap_1 -->
					<div class="foot_slide" ></div>
				</div>
			</div><!-- #home_slider -->
			<div id="topbar" class="topbar">
				<a class="home-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
					<h1 class="site-title"><?php bloginfo( 'name' ); ?></h1>
					<?php if(bloginfo( 'description' ) != "") { ?><h2 class="site-description"><?php bloginfo( 'description' ); ?></h2><?php } ?>
				</a>
				<div id="language" class="language">
					<!--<ul>-->
						<!--<li><a href="#"><img src="<?php //echo get_template_directory_uri(); ?>/images/lang-fr.png" alt="francais" /></a></li>
						<li><a href="#"><img src="<?php //echo get_template_directory_uri(); ?>/images/lang-en.png" alt="English" /></a></li>-->
						<!--<li class="select-header">-->
							<!--<select>
								<option>Selecitionner une langue</option>
							</select>-->
                            <div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'fr', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
						<!--</li>
					</ul>-->
				</div><!-- #language -->
				<div id="navbar" class="navbar">
					<nav id="site-navigation" class="navigation main-navigation" role="navigation">
						<h3 class="menu-toggle"><?php _e( 'Menu', 'twentythirteen' ); ?></h3>
						<a class="screen-reader-text skip-link" href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentythirteen' ); ?>"><?php _e( 'Skip to content', 'twentythirteen' ); ?></a>
						<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
						<?php // get_search_form(); ?>
					</nav><!-- #site-navigation -->
				</div><!-- #navbar -->
			</div><!-- #topbar -->
		</header><!-- #masthead -->

		<div id="main" class="site-main">
