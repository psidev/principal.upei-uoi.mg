<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/camera.css" type="text/css" media="screen"> 
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style6.css" />
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/camera.js"></script>
	<script language="javascript" type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/script.js"></script>
	<script>
		jQuery(document).ready(function(){   
              jQuery('.camera_wrap').camera();
			  
			var buttons = { previous:jQuery('#jslidernews3 .button-previous') ,
							next:jQuery('#jslidernews3 .button-next') };
	  
			var _complete = function(slider, index){ 
									jQuery('#jslidernews3 .slider-description').animate({height:0});
									slider.find(".slider-description").animate({height:100}) 
							};							;
			jQuery('#jslidernews3').lofJSidernews( { interval : 4000,
				direction		: 'opacity',	
				easing			: 'easeOutBounce',
				duration		: 1200,
				auto		 	: true,
				mainWidth:214,
				buttons			: buttons,
				onComplete:_complete 
			});	
        });    
	</script>
    <meta name="google-translate-customization" content="efb0f63ba785062f-c42e382e0cfbe856-g40109104eb3ff1de-10"></meta>
</head>

<body <?php body_class(); ?>>
	<div id="page" class="hfeed site">
		<header id="masthead" class="site-header" role="banner">			
			<div id="home_slider" >
				<div class="slider span8"> 
					<!-- D�but de #camera_wrap_1 -->
					<div class="camera_wrap camera_azure_skin" id="camera_wrap_1">
						<?php
							global $post;
							$postslist = get_posts('showposts=8&category_name=slide-home&order=DESC&orderby=date');						
							foreach($postslist as $post) :
						?>	
								<div data-thumb="<?php echo wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); ?>" data-src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); ?>">
									<div class="camera_caption fadeFromBottom">	
										<h3><?php //the_title(); ?></h3>
										<p><?php //echo $post->post_content; ?></p>
									</div>
								</div>
							<?php endforeach; 
								wp_reset_query(); 
							?>
					</div><!-- Fin de #camera_wrap_1 -->
					<div class="foot_slide" ></div>
				</div>
			</div><!-- #home_slider -->
			<div id="topbar" class="topbar">
				<a class="home-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
					<h1 class="site-title"><?php bloginfo( 'name' ); ?></h1>
					<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
				</a>
				<div id="language" class="language">
					<!--<ul>-->
						<!--<li><a href="#"><img src="<?php //echo get_template_directory_uri(); ?>/images/lang-fr.png" alt="fran�ais" /></a></li>
						<li><a href="#"><img src="<?php //echo get_template_directory_uri(); ?>/images/lang-en.png" alt="English" /></a></li>-->
						<!--<li class="select-header">-->
							<!--<select>
								<option>Selecitionner une langue</option>
							</select>-->
                            <div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'fr', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
						<!--</li>
					</ul>-->
				</div><!-- #language -->
				<div id="navbar" class="navbar">
					<nav id="site-navigation" class="navigation main-navigation" role="navigation">
						<h3 class="menu-toggle"><?php _e( 'Menu', 'twentythirteen' ); ?></h3>
						<a class="screen-reader-text skip-link" href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentythirteen' ); ?>"><?php _e( 'Skip to content', 'twentythirteen' ); ?></a>
						<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
						<?php // get_search_form(); ?>
					</nav><!-- #site-navigation -->
				</div><!-- #navbar -->
			</div><!-- #topbar -->
		</header><!-- #masthead -->

		<div id="main" class="site-main">
