<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php //language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php //language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php //language_attributes(); ?>translate="no">
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
    
    <meta content="index,follow,all" name="robots">
    
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style6.css" />
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/script.js"></script>
	<script>
		jQuery(document).ready(function(){   
            var buttons = { previous:jQuery('#jslidernews3 .button-previous') ,
							next:jQuery('#jslidernews3 .button-next') };
	  
			var _complete = function(slider, index){ 
									jQuery('#jslidernews3 .slider-description').animate({height:0});
									slider.find(".slider-description").animate({height:100}) 
							};							;
			jQuery('#jslidernews3').lofJSidernews( { interval : 4000,
				direction		: 'opacity',	
				easing			: 'easeOutBounce',
				duration		: 1200,
				auto		 	: true,
				mainWidth:214,
				buttons			: buttons,
				onComplete:_complete 
			});	
        });   

        //flash_annonce_randev
        function flash_annonce_randev_hide() { 
			jQuery("#flash_annonce_randev").hide(); 
		};
			 
	</script>

	<style type="text/css">
	.scroll-up {
		    background-color: #000000;
		    border: 1px solid rgba(0, 0, 0, 0.2);
		    bottom: 10px;
		    color: #ffffff;
		    display: none;
		    font-size: 10px;
		    height: auto;
		    padding: 9px;
		    position: fixed;
		    right: 25px;
		    text-align: left;
		    transition-duration: 0.5s;
		    width: 900px;
		    z-index: 1000;
		    font-size: 10px;
		}
		
	</style>

    <meta name="google-translate-customization" content="efb0f63ba785062f-c42e382e0cfbe856-g40109104eb3ff1de-10"/>
</head>

<body <?php body_class(); ?>>


<div style=" display: none; height:0px;">
Vanille Naturelle - STOI - Soci&eacute;t&eacute; Trading de l'Oc&eacute;an Indien, la STOI sp&eacute;cialis&eacute;e dans l&acute;agriculture biologique depuis 1997 avec activit&eacute; principale le respect de la terre, des producteurs, de la nature et des consommateurs, avec production du fertilisant biologique Taroka
<br />
vanille,naturelle,gousse,madagascar,vanille madagascar,agriculture,biologique,importation,agriculture biologique,terre,producteur,agriculteur,fertilisant biologique,fertilisant,plantation,grain sec,litchis,plantation de letchis, haricot,lingot blanc, haricot blanc, haricoot rouge,haricoot rouge marbr&eacute;,black eyes, arachides, zezika, taroka, c&eacute;rtif&eacute;e globalgap
<br />
Depuis 1999, elle a d&eacute;velopp&eacute; des plantations de vanille, de litchis, de girofle, de pomme de terre, de l&acute;ananas victoria, de piment dans les r&eacute;gions de Tamatave et d&acute;Antsirabe.
<br />
La STOI-AGRI est la premi&egrave;re &agrave; Madagascar &agrave; certifier leurs produits (fertilisants, litchis) en tant que produits biologiques aupr&egrave;s de l&acute;ECOCERT INTERNATIONAL.
<br />
Depuis 2006, sa production de vanille est &eacute;galement certifi&eacute;e bio.
<br />
Les plantations de litchis en partenariat avec des producteurs sont aussi certifi&eacute;es Global Gap &agrave; partir de 2007.
<br />
Durant plus de quinze ans, la STOI s&acute;est attach&eacute;e &agrave; promouvoir la diversit&eacute; des produits &agrave; Madagascar, la qualit&eacute; des produits r&eacute;gionaux, le savoir-faire des hommes dans le respect des cultures et traditions locales pour offrir au plus grand nombre ce que la nature peut donner de meilleur.
<br />
&quot; Le travail de la terre demande beaucoup d&acute;efforts. La collecte, le triage n&eacute;cessitent beaucoup de travail. Les conditions de travail &agrave; Madagascar ne sont pas toujours faciles. C&acute;est avec d&eacute;termination que beau temps mauvais temps, l&acute;&eacute;quipe met du c&oelig;ur &agrave; l&acute;ouvrage. Leurs efforts sont r&eacute;compens&eacute;s : leur plus grande satisfaction, ils l&acute;obtiennent quand ils voient le produit final sur les palettes. Le sentiment d&acute;accomplissement et de fiert&eacute; qu&acute;ils en tirent est une grande source de motivation &quot;
<br />
<?php
	global $post;
	$postslist_news = get_posts('showposts=1&category_name=NEWS&order=DESC&orderby=date');		
	//fb($postslist_news);
	$category_post = get_the_category($post->ID);
	if ($category_post[0]->name != "InfosAgriBio" && $category_post[0]->name != "NEWS")
	{
		foreach($postslist_news as $post_news) :
			echo $post_news->post_title."<br/>".$post_news->post_content;
		endforeach; 
	}
	wp_reset_query(); 
?>
<hr/>
<?php
	global $post;
	$postslist_agribio = get_posts('showposts=1&category_name=InfosAgriBio&order=DESC&orderby=date');		
	//fb($postslist_agribio);
	$category_post = get_the_category($post->ID);
	if ($category_post[0]->name != "InfosAgriBio" && $category_post[0]->name != "NEWS")
	{
		foreach($postslist_agribio as $post_agribio) :
			echo $post_agribio->post_title."<br/>".$post_agribio->post_content;
		endforeach; 
	}
	wp_reset_query(); 
?>
</div>


<?php
wp_reset_query(); 
global $post;
$page_id_current = $post->ID;
$parent_id_page_current = $post->post_parent;
$category_post = get_the_category($post->ID);
if ($parent_id_page_current=="114") $bg_header_to_show =  get_template_directory_uri()."/images/headers_litchis.jpg";
else if ($parent_id_page_current=="112") $bg_header_to_show =  get_template_directory_uri()."/images/headers.jpg";
else if ($parent_id_page_current=="116") $bg_header_to_show =  get_template_directory_uri()."/images/headers_grainsec.jpg";
else if ($parent_id_page_current=="118") $bg_header_to_show =  get_template_directory_uri()."/images/headers_taroka.jpg";
else if ($page_id_current=="276") $bg_header_to_show =  get_template_directory_uri()."/images/headers_arachide.jpg";
else if ($category_post[0]->name == "InfosAgriBio" || $category_post[0]->name == "NEWS" || $category_post[0]->name == "StoiAstuces") 
{
	$iii_rand_nb = rand(1, 6);
	if ($iii_rand_nb==1) $bg_header_to_show =  get_template_directory_uri()."/images/headers_arachide.jpg";
	else if ($iii_rand_nb==2) $bg_header_to_show =  get_template_directory_uri()."/images/headers_litchis.jpg";
	else if ($iii_rand_nb==3) $bg_header_to_show =  get_template_directory_uri()."/images/headers.jpg";
	else if ($iii_rand_nb==4) $bg_header_to_show =  get_template_directory_uri()."/images/headers_grainsec.jpg";
	else if ($iii_rand_nb==5) $bg_header_to_show =  get_template_directory_uri()."/images/headers_taroka.jpg";
	else if ($iii_rand_nb==6) $bg_header_to_show =  get_template_directory_uri()."/images/headers_default.jpg";
}
else $bg_header_to_show =  get_template_directory_uri()."/images/headers_default.jpg";
wp_reset_query(); 
?>

	<div id="page" class="hfeed site">
		<header id="masthead" class="site-header" role="banner">
			<div class="bg-page" style="overflow:hidden;border-bottom: medium solid;"><img src="<?php echo $bg_header_to_show; ?>" alt="entete" style="width:100%; height:auto;" /></div>
			<div id="topbar" class="topbar">
				<a class="home-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
					<h1 class="site-title"><?php bloginfo( 'name' ); ?></h1>
                    <?php if(bloginfo( 'description' ) != "") { ?><h2 class="site-description"><?php bloginfo( 'description' ); ?></h2><?php } ?>
				</a>				
				<div id="language" class="language">
					<!--<ul>-->
						<!--<li><a href="#"><img src="<?php //echo get_template_directory_uri(); ?>/images/lang-fr.png" alt="francais" /></a></li>
						<li><a href="#"><img src="<?php //echo get_template_directory_uri(); ?>/images/lang-en.png" alt="English" /></a></li>-->
						<!--<li class="select-header">-->
							<!--<select>
								<option>Selecitionner une langue</option>
							</select>-->
                            <div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'fr', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
						<!--</li>
					</ul>-->
				</div><!-- #language -->
				<div id="navbar" class="navbar">
					<nav id="site-navigation" class="navigation main-navigation" role="navigation">
						<h3 class="menu-toggle"><?php _e( 'Menu', 'twentythirteen' ); ?></h3>
						<a class="screen-reader-text skip-link" href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentythirteen' ); ?>"><?php _e( 'Skip to content', 'twentythirteen' ); ?></a>
						<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
						<?php // get_search_form(); ?>
					</nav><!-- #site-navigation -->
				</div><!-- #navbar -->
			</div><!-- #topbar -->
		</header><!-- #masthead -->

		<a class="scroll-up scroll" href="javascript:flash_annonce_randev_hide();" id="flash_annonce_randev" style="display: block;font-size: 10px;">
			<span style="float:right;">X</span>
			Madame, Monsieur,<br/>
			Nous vous informons du changement d’adresse du GROUPE STOI  depuis le 02 mars 2015.<br/>
			Les nouveaux locaux sont situés  a Fort Duchesne  (tout en haut de la descente en pavée qui va vers Tsiadana Ampasanimalo, 1ere bifurcation a gauche, 1ère maison ).<br/>
			Notre ADRESSE sera : <br/>
			BP 8582 
			FORT DUCHESNE 
			Villa ARMEL 
			101 ANTANANARIVO<br/>
			Vous voudrez noter que nos numéros de ligne TELMA sont en attendant : + 261 20 22 310 42/36 . Nous sommes également joignables sur le numéro de mobile +261 (0) 34 15 597 21.<br/>
			Vos interlocuteurs habituels se tiendront à votre disposition pour vous accueillir.
		</a>

		<div id="main" class="site-main">
